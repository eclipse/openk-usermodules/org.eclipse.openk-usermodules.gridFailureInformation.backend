/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.mailexport.config;

import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Profile("!test")
@Configuration
@Log4j2
public class RabbitMqConfig {

    // RabbitMQ Configurationen für Mail-Export
    @Value("${spring.rabbitmq.export_exchange}")
    public String exportExchangeName;
    @Value("${spring.rabbitmq.mail_export_queue}")
    public String mailExportQueueName;
    @Value("${spring.rabbitmq.mail_export_routingkey}")
    public String mailExportRoutingKeyName;

    @Bean
    public DirectExchange exportExchange() {
        log.info("************** Configure RabbitMQ **************");
        log.info("ExportExchange: " + exportExchangeName);
        return new DirectExchange(exportExchangeName);
    }

    @Bean
    public Queue mailExportQueue() {
        log.info("ExportQueue: " + mailExportQueueName);
        return new Queue(mailExportQueueName); }

    @Bean
    public Binding mailExportBinding() {
        log.info("ExportKey: " + mailExportRoutingKeyName);
        return BindingBuilder.bind(mailExportQueue()).to(exportExchange()).with(mailExportRoutingKeyName); }

}