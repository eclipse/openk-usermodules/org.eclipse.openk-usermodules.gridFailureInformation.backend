/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.config;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.StoerungsauskunftInterfaceApplication;
import org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.api.StoerungsauskunftApi;
import org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.dtos.StoerungsauskunftUserNotification;
import org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.service.ImportExportService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = StoerungsauskunftInterfaceApplication.class)
@ContextConfiguration(classes = {TestConfiguration.class})
@ActiveProfiles("test")
public class ImportSchedulerConfigTest {
    @Autowired
    @SpyBean
    private ImportExportService importExportService;

    @Autowired
    private StoerungsauskunftApi stoerungsauskunftApi;

    @Autowired
    private ImportSchedulerConfig importSchedulerConfig;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void shoulImportUserNotification() throws IOException {
        InputStream is = new ClassPathResource("UsernotificationJsonResponse.json").getInputStream();
        List<StoerungsauskunftUserNotification> userNotificationList =
                objectMapper.readValue(is, new TypeReference<>() {
                });
        when(stoerungsauskunftApi.getUserNotification(any(Integer.class))).thenReturn(userNotificationList);

        importSchedulerConfig.scheduleTaskImportUserNotifications();
        verify(importExportService, times(1)).importUserNotifications();
    }
}
