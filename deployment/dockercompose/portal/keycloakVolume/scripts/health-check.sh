#!/bin/sh

# Function to open a network connection to a specified host and port
open_network_connection() {
    domain="$1"
    port="$2"
    exec 3<>/dev/tcp/"$domain"/"$port"
}

# Check if all three parameters are provided as arguments
if [ $# -ne 3 ]; then
    echo "Usage: $0 <domain> <port> <GET_URL>"
    exit 1
fi

# Call the function with the provided domain and port arguments
open_network_connection "$1" "$2"

# Now you can use file descriptor 3 to read from and write to the network connection
echo "GET $3 HTTP/1.1" >&3
echo "Host: $1" >&3
echo >&3

timeout --preserve-status 1 cat <&3 | grep -m 1 status | grep -m 1 UP
ERROR=$?

# Close the network connection
exec 3<&-
exec 3>&-

exit $ERROR