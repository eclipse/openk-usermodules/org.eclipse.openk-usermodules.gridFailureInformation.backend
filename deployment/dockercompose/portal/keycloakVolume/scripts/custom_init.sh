#!/bin/bash
echo "Custom Start"
if [ "$OVERWRITE_REALM" = "true" ]; then
  echo "Importing realm (overwriting old)"
  /opt/keycloak/bin/kc.sh --verbose import --file /opt/keycloak/data/import/realm.json --override true --optimized
fi
/opt/keycloak/bin/kc.sh show-config
/opt/keycloak/bin/kc.sh start --import-realm --optimized
