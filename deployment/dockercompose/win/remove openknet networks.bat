@echo off
for /f "tokens=*" %%n in ('docker network ls --quiet --filter "name=openknet"') do (
    docker network rm %%n
)