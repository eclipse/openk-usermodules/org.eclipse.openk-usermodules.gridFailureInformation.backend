package org.eclipse.openk.gridfailureinformation.repository;

import org.eclipse.openk.gridfailureinformation.model.TblDistributionGroupAllowed;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface DistributionGroupAllowedRepository extends JpaRepository<TblDistributionGroupAllowed, Long > {

    List<TblDistributionGroupAllowed> findAll();

    List<TblDistributionGroupAllowed> findByDistributionGroupId(UUID uuid);
}
