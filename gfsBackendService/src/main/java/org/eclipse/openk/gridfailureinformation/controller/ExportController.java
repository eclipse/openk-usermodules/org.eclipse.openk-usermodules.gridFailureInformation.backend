/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.gridfailureinformation.email.EmailService;
import org.eclipse.openk.gridfailureinformation.service.ExportService;
import org.eclipse.openk.gridfailureinformation.service.ImportGeoJsonService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jakarta.mail.MessagingException;
import java.util.UUID;


@Log4j2
@RestController
@RequestMapping("/export")
public class ExportController {
    private final ExportService exportService;
    private final ImportGeoJsonService importGeoJsonService;
    private final EmailService emailService;

    public ExportController(ExportService exportService, ImportGeoJsonService importGeoJsonService, EmailService emailService) {
        this.exportService = exportService;
        this.importGeoJsonService = importGeoJsonService;
        this.emailService = emailService;
    }


    @Secured({"ROLE_GRID-FAILURE-ADMIN", "ROLE_GRID-FAILURE-CREATOR"})
    @PutMapping("/{failureInfoUuid}")
    @Operation(summary = "Export einer Störungsinformation über die übergebenen Kanäle")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Störungsinformation erfolgreich exportiert"),
            @ApiResponse(responseCode = "500", description = "Konnte nicht durchgeführt werden")
    })
    public ResponseEntity<Void> exportFailureInformation(@PathVariable UUID failureInfoUuid, @RequestBody String[] channels) {
        exportService.exportFailureInformation(failureInfoUuid, channels, null);
        return ResponseEntity.ok().build();
    }

    @Secured({"ROLE_GRID-FAILURE-ADMIN", "ROLE_GRID-FAILURE-CREATOR"})
    @GetMapping("/exporttodmz")
    @Operation(summary = "Export der veröffentlichten Störungsinformation")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Störungsinformation erfolgreich exportiert"),
            @ApiResponse(responseCode = "500", description = "Konnte nicht durchgeführt werden")
    })
    public ResponseEntity<Void> exportFailureInformationsToDMZ () {
        exportService.exportFeSettingsToDMZ();
        exportService.exportFailureInformationsToDMZ();
        return ResponseEntity.ok().build();
    }

    @Secured({"ROLE_GRID-FAILURE-ADMIN", "ROLE_GRID-FAILURE-CREATOR"})
    @GetMapping("/importgeojson")
    @Operation(summary = "Import der GeoJsons für die Stationen")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "GeoJsons erfolgreich importiert"),
            @ApiResponse(responseCode = "500", description = "Konnte nicht durchgeführt werden")
    })
    public ResponseEntity<Void> importGeoJsonTask() {
        importGeoJsonService.importGeoJsonFile();
        return ResponseEntity.ok().build();
    }

    @Secured({"ROLE_GRID-FAILURE-ADMIN", "ROLE_GRID-FAILURE-CREATOR"})
    @Operation(summary = "Versende Testmail")
    @ApiResponses(value = {@ApiResponse(responseCode = "204", description = "Erfolgreich durchgeführt")})
    @GetMapping("/testmail")
    public ResponseEntity<Void> testMail() throws MessagingException {
        emailService.sendTestMail("test@test33.de");
        return ResponseEntity.ok().build();
    }
}
