/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.gridfailureinformation.util;

import org.eclipse.openk.gridfailureinformation.bpmn.impl.GfiProcessState;
import org.eclipse.openk.gridfailureinformation.constants.Constants;
import org.eclipse.openk.gridfailureinformation.exceptions.NotFoundException;
import org.eclipse.openk.gridfailureinformation.repository.StatusRepository;
import org.eclipse.openk.gridfailureinformation.viewmodel.FailureInformationDto;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import static org.eclipse.openk.gridfailureinformation.constants.Constants.EXT_STATUS_FINISHED;
import static org.eclipse.openk.gridfailureinformation.constants.Constants.EXT_STATUS_IN_WORK;
import static org.eclipse.openk.gridfailureinformation.constants.Constants.EXT_STATUS_PLANNED;

public class ExternalStatusCalculator {
    private ExternalStatusCalculator() {}

    public static String addExternalStatus(StatusRepository statusRepository, FailureInformationDto failureInformationDto){
        Long statusInternId = statusRepository.findByUuid(
                failureInformationDto.getStatusInternId())
                .orElseThrow(() -> new NotFoundException("status.not.found"))
                .getId();
        String publicationStatus = failureInformationDto.getPublicationStatus();
        Date failureBegin = failureInformationDto.getFailureBegin();
        Date failureEndPlanned = failureInformationDto.getFailureEndPlanned();
        LocalDateTime ldtFailureBegin = null;
        LocalDateTime ldtFailureEndPlanned = null;


        if(failureBegin != null) {
            ldtFailureBegin = LocalDateTime.ofInstant(failureBegin.toInstant(), ZoneId.systemDefault());
        }

        if(failureEndPlanned != null) {
            ldtFailureEndPlanned = LocalDateTime.ofInstant(failureEndPlanned.toInstant(), ZoneId.systemDefault());
        }

        String statusExtern = ExternalStatusCalculator.calculate(statusInternId, publicationStatus, ldtFailureBegin,
                ldtFailureEndPlanned);
        failureInformationDto.setStatusExtern(statusExtern);
        return statusExtern;
    }

    public static String calculate(Long statusInternId, String publicationStatus, LocalDateTime ldtFailureBegin, LocalDateTime ldtFailureEndPlanned) {
        LocalDateTime ltdToday =  java.time.LocalDateTime.now();
        String newStatusText = "";

        if ((statusInternId.equals(GfiProcessState.QUALIFIED.getStatusValue())
                || statusInternId.equals(GfiProcessState.UPDATED.getStatusValue())
                || Constants.PUB_STATUS_VEROEFFENTLICHT.equalsIgnoreCase(publicationStatus))
                && ldtFailureEndPlanned != null && ldtFailureEndPlanned.isBefore(ltdToday)) {
            newStatusText = EXT_STATUS_FINISHED;
        }
        else if ((statusInternId.equals(GfiProcessState.QUALIFIED.getStatusValue())
                || statusInternId.equals(GfiProcessState.UPDATED.getStatusValue()))
                && ldtFailureBegin != null && ldtFailureBegin.isAfter(ltdToday)) {
            newStatusText = EXT_STATUS_PLANNED;
        }
        else if ((statusInternId.equals(GfiProcessState.QUALIFIED.getStatusValue())
                || statusInternId.equals(GfiProcessState.UPDATED.getStatusValue()))
                && ldtFailureBegin != null && ldtFailureBegin.isBefore(ltdToday)) {
            newStatusText = EXT_STATUS_IN_WORK;
        } else if (statusInternId.equals(GfiProcessState.CANCELED.getStatusValue())
                || statusInternId.equals(GfiProcessState.COMPLETED.getStatusValue())) {
            newStatusText = EXT_STATUS_FINISHED;
        }
        return newStatusText;
    }
}
