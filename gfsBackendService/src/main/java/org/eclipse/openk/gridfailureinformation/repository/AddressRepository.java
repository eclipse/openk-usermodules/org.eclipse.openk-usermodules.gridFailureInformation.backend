/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */

package org.eclipse.openk.gridfailureinformation.repository;

import org.eclipse.openk.gridfailureinformation.model.TblAddress;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface AddressRepository extends JpaRepository<TblAddress, Long > {

    List<TblAddress> findAll();

    Optional<TblAddress> findByUuid(UUID uuid);

    List<TblAddress> findByStationId(String stationId);

    List<TblAddress> findByPowerConnection(Boolean powerConnection);
    List<TblAddress> findByWaterConnection(Boolean waterConnection);
    List<TblAddress> findByGasConnection(Boolean gasConnection);
    List<TblAddress> findByDistrictheatingConnection(Boolean districtheationConnection);
    List<TblAddress> findByTelecommConnection(Boolean telecommConnection);

    @Query("select distinct a.community from TblAddress a")
    List<String> findAllCommunitys();
    @Query("select distinct a.community from TblAddress a where powerConnection = true")
    List<String> findAllCommunitysForPower();
    @Query("select distinct a.community from TblAddress a where gasConnection = true")
    List<String> findAllCommunitysForGas();
    @Query("select distinct a.community from TblAddress a where waterConnection = true")
    List<String> findAllCommunitysForWater();
    @Query("select distinct a.community from TblAddress a where districtheatingConnection = true")
    List<String> findAllCommunitysForDistrictheating();
    @Query("select distinct a.community from TblAddress a where telecommConnection = true")
    List<String> findAllCommunitysForTelecomm();
    @Query("select distinct a from TblAddress a where postcode = :postcode and community = :community and district = :district and street = :street and housenumber = :housenumber")
    List<TblAddress> findByCompleteAdress(String postcode, String community, String district, String street, String housenumber);

    @Query("select distinct a.district from TblAddress a where community = :community")
    List<String> findDistrictsByCommunity(@Param("community") String community);
    @Query("select distinct a.district from TblAddress a where community = :community and powerConnection = true")
    List<String> findDistrictsByCommunityForPowerConnections(@Param("community") String community);
    @Query("select distinct a.district from TblAddress a where community = :community and gasConnection = true")
    List<String> findDistrictsByCommunityForGasConnections(@Param("community") String community);
    @Query("select distinct a.district from TblAddress a where community = :community and waterConnection = true")
    List<String> findDistrictsByCommunityForWaterConnections(@Param("community") String community);
    @Query("select distinct a.district from TblAddress a where community = :community and districtheatingConnection = true")
    List<String> findDistrictsByCommunityForDistrictheatingConnections(@Param("community") String community);
    @Query("select distinct a.district from TblAddress a where community = :community and telecommConnection = true")
    List<String> findDistrictsByCommunityForTelecommConnections(@Param("community") String community);

    @Query("select distinct a.postcode from TblAddress a where community = :community and district = :district")
    List<String> findPostcodesByCommunityAndDistrict(@Param("community") String community, @Param("district") String district);
    @Query("select distinct a.postcode from TblAddress a where community = :community and district = :district and powerConnection = true")
    List<String> findPostcodesByCommunityAndDistrictForPower(@Param("community") String community, @Param("district") String district);
    @Query("select distinct a.postcode from TblAddress a where community = :community and district = :district and gasConnection = true")
    List<String> findPostcodesByCommunityAndDistrictForGas(@Param("community") String community, @Param("district") String district);
    @Query("select distinct a.postcode from TblAddress a where community = :community and district = :district and waterConnection = true")
    List<String> findPostcodesByCommunityAndDistrictForWater(@Param("community") String community, @Param("district") String district);
    @Query("select distinct a.postcode from TblAddress a where community = :community and district = :district and districtheatingConnection = true")
    List<String> findPostcodesByCommunityAndDistrictForDistrictheating(@Param("community") String community, @Param("district") String district);
    @Query("select distinct a.postcode from TblAddress a where community = :community and district = :district and telecommConnection = true")
    List<String> findPostcodesByCommunityAndDistrictForTelecomm(@Param("community") String community, @Param("district") String district);

    @Query("select distinct a.postcode from TblAddress a")
    List<String> findAllPostcodes();
    @Query("select distinct a.postcode from TblAddress a where powerConnection = true")
    List<String> findAllPostcodesForPower();
    @Query("select distinct a.postcode from TblAddress a where gasConnection = true")
    List<String> findAllPostcodesForGas();
    @Query("select distinct a.postcode from TblAddress a where waterConnection = true")
    List<String> findAllPostcodesForWater();
    @Query("select distinct a.postcode from TblAddress a where districtheatingConnection = true")
    List<String> findAllPostcodesForDistrictheating();
    @Query("select distinct a.postcode from TblAddress a where telecommConnection = true")
    List<String> findAllPostcodesForTelecomm();

    @Query("select distinct a.community from TblAddress a where postcode = :postcode")
    List<String> findCommunitiesByPostcode(@Param("postcode") String postcode);
    @Query("select distinct a.community from TblAddress a where postcode = :postcode and powerConnection = true")
    List<String> findCommunitiesByPostcodeForPowerConnections(@Param("postcode") String postcode);
    @Query("select distinct a.community from TblAddress a where postcode = :postcode and gasConnection = true")
    List<String> findCommunitiesByPostcodeForGasConnections(@Param("postcode") String postcode);
    @Query("select distinct a.community from TblAddress a where postcode = :postcode and waterConnection = true")
    List<String> findCommunitiesByPostcodeForWaterConnections(@Param("postcode") String postcode);
    @Query("select distinct a.community from TblAddress a where postcode = :postcode and districtheatingConnection = true")
    List<String> findCommunitiesByPostcodeForDistrictheatingConnections(@Param("postcode") String postcode);
    @Query("select distinct a.community from TblAddress a where postcode = :postcode and telecommConnection = true")
    List<String> findCommunitiesByPostcodeForTelecommConnections(@Param("postcode") String postcode);

    @Query("select distinct a.district from TblAddress a where postcode = :postcode and community = :community")
    List<String> findDistrictsByPostcodeAndCommunity(@Param("postcode") String postcode, @Param("community") String community);
    @Query("select distinct a.district from TblAddress a where postcode = :postcode and community = :community and powerConnection = true")
    List<String> findDistrictsByPostcodeAndCommunityForPower(@Param("postcode") String postcode, @Param("community") String community);
    @Query("select distinct a.district from TblAddress a where postcode = :postcode and community = :community and gasConnection = true")
    List<String> findDistrictsByPostcodeAndCommunityForGas(@Param("postcode") String postcode, @Param("community") String community);
    @Query("select distinct a.district from TblAddress a where postcode = :postcode and community = :community and waterConnection = true")
    List<String> findDistrictsByPostcodeAndCommunityForWater(@Param("postcode") String postcode, @Param("community") String community);
    @Query("select distinct a.district from TblAddress a where postcode = :postcode and community = :community and districtheatingConnection = true")
    List<String> findDistrictsByPostcodeAndCommunityForDistrictheating(@Param("postcode") String postcode, @Param("community") String community);
    @Query("select distinct a.district from TblAddress a where postcode = :postcode and community = :community and telecommConnection = true")
    List<String> findDistrictsByPostcodeAndCommunityForTelecomm(@Param("postcode") String postcode, @Param("community") String community);

    @Query("select distinct a.street from TblAddress a where postcode = :postcode and community = :community")
    List<String> findStreetsByPostcodeAndCommunity(@Param("postcode") String postcode, @Param("community") String community);
    @Query("select distinct a.street from TblAddress a where postcode = :postcode and community = :community and powerConnection = true")
    List<String> findStreetsByPostcodeAndCommunityForPower(@Param("postcode") String postcode, @Param("community") String community);
    @Query("select distinct a.street from TblAddress a where postcode = :postcode and community = :community and gasConnection = true")
    List<String> findStreetsByPostcodeAndCommunityForGas(@Param("postcode") String postcode, @Param("community") String community);
    @Query("select distinct a.street from TblAddress a where postcode = :postcode and community = :community and waterConnection = true")
    List<String> findStreetsByPostcodeAndCommunityForWater(@Param("postcode") String postcode, @Param("community") String community);
    @Query("select distinct a.street from TblAddress a where postcode = :postcode and community = :community and districtheatingConnection = true")
    List<String> findStreetsByPostcodeAndCommunityForDistrictheating(@Param("postcode") String postcode, @Param("community") String community);
    @Query("select distinct a.street from TblAddress a where postcode = :postcode and community = :community and telecommConnection = true")
    List<String> findStreetsByPostcodeAndCommunityForTelecomm(@Param("postcode") String postcode, @Param("community") String community);

    @Query("select distinct a.street from TblAddress a where postcode = :postcode and community = :community and district = :district")
    List<String> findStreetsByPostcodeAndCommunityAndDistrict(@Param("postcode") String postcode, @Param("community") String community, @Param("district") String district);
    @Query("select distinct a.street from TblAddress a where postcode = :postcode and community = :community and district = :district and powerConnection = true")
    List<String> findStreetsByPostcodeAndCommunityAndDistrictForPower(@Param("postcode") String postcode, @Param("community") String community, @Param("district") String district);
    @Query("select distinct a.street from TblAddress a where postcode = :postcode and community = :community and district = :district and gasConnection = true")
    List<String> findStreetsByPostcodeAndCommunityAndDistrictForGas(@Param("postcode") String postcode, @Param("community") String community, @Param("district") String district);
    @Query("select distinct a.street from TblAddress a where postcode = :postcode and community = :community and district = :district and waterConnection = true")
    List<String> findStreetsByPostcodeAndCommunityAndDistrictForWater(@Param("postcode") String postcode, @Param("community") String community, @Param("district") String district);
    @Query("select distinct a.street from TblAddress a where postcode = :postcode and community = :community and district = :district and districtheatingConnection = true")
    List<String> findStreetsByPostcodeAndCommunityAndDistrictForDistrictHeating(@Param("postcode") String postcode, @Param("community") String community, @Param("district") String district);
    @Query("select distinct a.street from TblAddress a where postcode = :postcode and community = :community and district = :district and telecommConnection = true")
    List<String> findStreetsByPostcodeAndCommunityAndDistrictForTelecomm(@Param("postcode") String postcode, @Param("community") String community, @Param("district") String district);

    List<TblAddress> findByPostcodeAndCommunityAndStreet(String postcode, String community, String street);
    @Query("select a from TblAddress a where postcode = :postcode and community = :community and street = :street and powerConnection = true")
    List<TblAddress> findByPostcodeAndCommunityAndStreetForPower(String postcode, String community, String street);
    @Query("select a from TblAddress a where postcode = :postcode and community = :community and street = :street and gasConnection = true")
    List<TblAddress> findByPostcodeAndCommunityAndStreetForGas(String postcode, String community, String street);
    @Query("select a from TblAddress a where postcode = :postcode and community = :community and street = :street and waterConnection = true")
    List<TblAddress> findByPostcodeAndCommunityAndStreetForWater(String postcode, String community, String street);
    @Query("select a from TblAddress a where postcode = :postcode and community = :community and street = :street and districtheatingConnection = true")
    List<TblAddress> findByPostcodeAndCommunityAndStreetForDistrictheating(String postcode, String community, String street);
    @Query("select a from TblAddress a where postcode = :postcode and community = :community and street = :street and telecommConnection = true")
    List<TblAddress> findByPostcodeAndCommunityAndStreetForTelecomm(String postcode, String community, String street);
}
