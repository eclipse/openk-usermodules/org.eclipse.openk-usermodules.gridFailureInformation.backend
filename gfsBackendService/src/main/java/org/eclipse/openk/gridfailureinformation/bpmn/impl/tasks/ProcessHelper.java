/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.bpmn.impl.tasks;

import lombok.Data;
import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.gridfailureinformation.bpmn.base.ProcessException;
import org.eclipse.openk.gridfailureinformation.bpmn.base.ProcessState;
import org.eclipse.openk.gridfailureinformation.bpmn.impl.GfiGrid;
import org.eclipse.openk.gridfailureinformation.bpmn.impl.GfiProcessEnvironment;
import org.eclipse.openk.gridfailureinformation.bpmn.impl.GfiProcessState;
import org.eclipse.openk.gridfailureinformation.bpmn.impl.GfiProcessSubject;
import org.eclipse.openk.gridfailureinformation.config.rabbitmq.RabbitMqConfig;
import org.eclipse.openk.gridfailureinformation.exceptions.BadRequestException;
import org.eclipse.openk.gridfailureinformation.exceptions.InternalServerErrorException;
import org.eclipse.openk.gridfailureinformation.exceptions.NotFoundException;
import org.eclipse.openk.gridfailureinformation.model.RefStatus;
import org.eclipse.openk.gridfailureinformation.model.TblFailureInformation;
import org.eclipse.openk.gridfailureinformation.model.TblFailureInformationPublicationChannel;
import org.eclipse.openk.gridfailureinformation.viewmodel.FailureInformationDto;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Log4j2
@Component
@Transactional()
@Data
public class ProcessHelper {
    private final RabbitMqConfig rabbitMqConfig;

    private final GfiProcessEnvironment environment;

    private final GfiGrid grid;

    public ProcessHelper(RabbitMqConfig rabbitMqConfig, GfiProcessEnvironment environment, GfiGrid grid) {
        this.rabbitMqConfig = rabbitMqConfig;
        this.environment = environment;
        this.grid = grid;
    }

    @Transactional
    public FailureInformationDto updateFailureInfo(FailureInformationDto failureInfoDto) {
        return processGrid(failureInfoDto, null);
    }

    @Transactional
    public FailureInformationDto storeFailureFromViewModel(FailureInformationDto dto) {
        return environment.getFailureInformationService().storeFailureInfo(dto, GfiProcessState.NEW);
    }

    @Transactional
    public FailureInformationDto updateAndPublish(FailureInformationDto failureInformationDto) {
        if (environment.getFailureInformationService().getRefStatus(failureInformationDto).getId() != GfiProcessState.QUALIFIED.getStatusValue()) {
            throw new BadRequestException("Publishing is only possible with state 'qualified'");
        }
        TblFailureInformation tblFailureInfo = environment.getFailureInformationService().getFailureInformationRepository().findByUuid(failureInformationDto.getUuid()).orElseThrow(()-> new NotFoundException(""));
        List<TblFailureInformationPublicationChannel>  channelList = environment.getFailureInformationService().getFailureInformationPublicationChannelRepository().findByTblFailureInformation(tblFailureInfo);
        if (channelList.isEmpty()) {
            throw new BadRequestException("Publishing is only possible with at least one selected channel");
        }
        return processGrid(failureInformationDto, GfiProcessState.CHECKED_FOR_PUBLISH_);
    }

    public FailureInformationDto processGrid(FailureInformationDto failureInfoDto, GfiProcessState forcedState) {
        GfiProcessSubject subject = GfiProcessSubject.of(failureInfoDto, this);

        if(forcedState != null) {
            subject.setStateInDb(forcedState); // use forced state
        }
        else {
            // use state from db
            RefStatus refStatus = environment.getFailureInformationService().getRefStatus(failureInfoDto);
            subject.setStateInDb(GfiProcessState.fromValue(refStatus.getId()));
        }
        try {
            grid.recover(subject).start(subject::getStateInDb);
        } catch (ProcessException e) {
            log.error(e.getMessage() + " -> \r\n" + Arrays.toString(e.getStackTrace()));
            throw new InternalServerErrorException(e.getMessage());
        }

        return subject.getFailureInformationDto();
    }


    public ProcessState getProcessStateFromStatusUuid(UUID statusUuid) {
        Optional<RefStatus> optRefStatus = environment.getStatusRepository().findByUuid(statusUuid);
        if(optRefStatus.isEmpty()) {
            log.error("RefStatus <"+statusUuid+"> not found in DB");
            throw  new InternalServerErrorException("status.uuid.not.found");
        }
        return GfiProcessState.fromValue(optRefStatus.get().getId());
    }

    public FailureInformationDto storeEditStatus( FailureInformationDto dto, GfiProcessState newState ) {
        UUID statusUuid = environment.getStatusRepository()
                .findById(newState.getStatusValue())
                .orElseThrow( () -> new NotFoundException( "status.not.found"))
                .getUuid();

        dto.setStatusInternId(statusUuid);
        return environment.getFailureInformationService().storeFailureInfo(dto, GfiProcessState.NEW);
    }

    public void publishFailureInformation(FailureInformationDto dto, boolean onlyMail, GfiProcessState processState) {
        List<String> finalChannelList = environment.getFailureInformationService().getChannelsToPublishList(dto.getUuid(), onlyMail);
        environment.getExportService().exportFailureInformation(dto.getUuid(), finalChannelList.toArray(new String[0]), processState);
    }

    public void resetPublishedStateForChannels( FailureInformationDto dto ) {
        TblFailureInformation tblFailureInformation = environment.getFailureInformationRepository().findByUuid(dto.getUuid())
                .orElseThrow(() -> new NotFoundException("failure.information.not.found"));

        List<TblFailureInformationPublicationChannel> channelList = environment.getChannelRepository().findByTblFailureInformation(tblFailureInformation);

        channelList.forEach(x -> {
            x.setPublished(false);
            environment.getChannelRepository().save(x);
        });
    }

    public List<FailureInformationDto> getSubordinatedChildren(FailureInformationDto dto ) {
        return environment.getFailureInformationService().findFailureInformationsByCondensedUuid(dto.getUuid());
    }
}
