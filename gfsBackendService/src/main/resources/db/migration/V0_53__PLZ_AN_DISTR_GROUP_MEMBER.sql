﻿-----------------------------------------------------------------------------------
-- *******************************************************************************
-- * Copyright (c) 2019 Contributors to the Eclipse Foundation
-- *
-- * See the NOTICE file(s) distributed with this work for additional
-- * information regarding copyright ownership.
-- *
-- * This program and the accompanying materials are made available under the
-- * terms of the Eclipse Public License v. 2.0 which is available at
-- * http://www.eclipse.org/legal/epl-2.0.
-- *
-- * SPDX-License-Identifier: EPL-2.0
-- *******************************************************************************
-----------------------------------------------------------------------------------

DO $$
    BEGIN
        BEGIN
            ALTER TABLE public.TBL_DISTRIBUTION_GROUP_MEMBER ADD COLUMN POSTCODES varchar(1024);
        EXCEPTION
            WHEN duplicate_column THEN RAISE NOTICE 'column POSTCODES already exists in TBL_DISTRIBUTION_GROUP_MEMBER.';
        END;
    END;
$$
