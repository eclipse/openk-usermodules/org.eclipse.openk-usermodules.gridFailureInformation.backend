/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.jobs.importgridfailures.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.eclipse.openk.gridfailureinformation.jobs.importgridfailures.TestImportGridFailuresApplication;
import org.eclipse.openk.gridfailureinformation.jobs.importgridfailures.config.TestConfiguration;
import org.eclipse.openk.gridfailureinformation.jobs.importgridfailures.dtos.ForeignFailureMessageDto;
import org.eclipse.openk.gridfailureinformation.jobs.importgridfailures.service.ImportService;
import org.eclipse.openk.gridfailureinformation.jobs.importgridfailures.support.MockDataHelper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.MediaType;
import org.springframework.messaging.MessageChannel;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = TestImportGridFailuresApplication.class)
@ContextConfiguration(classes = {TestConfiguration.class})
@ActiveProfiles("test")
public class ImportControllerTest {
    @Autowired
    private ImportService importService;

    private static MockMvc mockMvc;

    @BeforeEach
    public void init() {
        mockMvc = MockMvcBuilders
                .standaloneSetup(new ImportController(importService))
                .build();
    }

    @Test
    public void shouldCallImport() throws Exception {
        ForeignFailureMessageDto dto = MockDataHelper.mockForeignFailureDto();

        mockMvc.perform(post("/import")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(dto)))
                .andExpect(status().is2xxSuccessful());
    }
}
