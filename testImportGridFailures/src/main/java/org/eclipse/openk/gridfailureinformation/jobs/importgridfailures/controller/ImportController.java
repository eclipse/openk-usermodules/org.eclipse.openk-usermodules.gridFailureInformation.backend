/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.jobs.importgridfailures.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.gridfailureinformation.jobs.importgridfailures.dtos.ForeignFailureMessageDto;
import org.eclipse.openk.gridfailureinformation.jobs.importgridfailures.service.ImportService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@Log4j2
@RestController
@RequestMapping("/import")
public class ImportController {
    private final ImportService importService;

    public ImportController(ImportService importService) {
        this.importService = importService;
    }

    @PostMapping
    @Operation(summary = "Import einer externen Störungsinformation")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Störungsinformation erfolgreich importiert"),
            @ApiResponse(responseCode = "500", description = "Konnte nicht durchgeführt werden")
    })
    public void pushForeignFailureInfo(@Validated @RequestBody ForeignFailureMessageDto foreignFailureMessageDto) {
        importService.pushForeignFailure(foreignFailureMessageDto);
    }
}
