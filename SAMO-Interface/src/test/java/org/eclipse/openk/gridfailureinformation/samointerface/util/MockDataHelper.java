package org.eclipse.openk.gridfailureinformation.samointerface.util;

import org.eclipse.openk.gridfailureinformation.samointerface.dtos.StoerungsauskunftUserNotification;

import java.util.UUID;

public class MockDataHelper {

    public static StoerungsauskunftUserNotification mockStoerungsauskunftUserNotification() {
        StoerungsauskunftUserNotification notification = new StoerungsauskunftUserNotification();

        notification.setId(UUID.randomUUID().toString());
        notification.setComment("Test-Comment");
        notification.setDistrict("Test-District");
        notification.setCity("Test-City");
        notification.setStreet("Test-Street");
        notification.setHouseNo("Test-HouseNo");

        notification.setLng("8.214552");
        notification.setLat("53.143452");
        notification.setDate("28.5.2020 14:49:11");

        return notification;
    }
}
