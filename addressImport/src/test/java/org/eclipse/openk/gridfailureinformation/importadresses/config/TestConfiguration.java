/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.importadresses.config;

import org.eclipse.openk.gridfailureinformation.importadresses.AddressImportApplication;
import org.eclipse.openk.gridfailureinformation.importadresses.jobs.JobManager;
import org.eclipse.openk.gridfailureinformation.importadresses.mapper.AddressMapper;
import org.eclipse.openk.gridfailureinformation.importadresses.mapper.AddressMapperImpl;
import org.eclipse.openk.gridfailureinformation.importadresses.mapper.StationMapper;
import org.eclipse.openk.gridfailureinformation.importadresses.mapper.StationMapperImpl;
import org.eclipse.openk.gridfailureinformation.importadresses.repository.AddressRepository;
import org.eclipse.openk.gridfailureinformation.importadresses.repository.StationRepository;
import org.eclipse.openk.gridfailureinformation.importadresses.service.AddressImportService;
import org.eclipse.openk.gridfailureinformation.importadresses.service.AddressService;
import org.eclipse.openk.gridfailureinformation.importadresses.service.StationService;
import org.eclipse.openk.gridfailureinformation.importadresses.util.UtmConverter;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.flyway.FlywayAutoConfiguration;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.ConfigDataApplicationContextInitializer;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

@EnableJpaRepositories(basePackages = "org.eclipse.openk.gridfailureinformation.importadresses")
@EntityScan(basePackageClasses = AddressImportApplication.class)
@ContextConfiguration(initializers = {ConfigDataApplicationContextInitializer.class})
@org.springframework.boot.test.context.TestConfiguration
public class TestConfiguration {
    @Bean
    public AddressMapper addressMapper() { return new AddressMapperImpl(); }

    @Bean
    public StationMapper stationMapper() { return new StationMapperImpl(); }

    @Bean
    public UtmConverter utmConverter() { return new UtmConverter(); }

    @MockBean
    private AddressRepository addressRepository;

    @Bean
    public AddressService addressService() {
        return new AddressService(addressRepository, addressMapper());
    }

    @Bean
    public AddressImportService addressImportService() {
        return new AddressImportService(
                addressService(),
                stationService(),
                utmConverter()
        );
    }

    @MockBean
    private StationRepository stationRepository;

    @Bean
    public StationService stationService() {
        return new StationService(stationRepository, stationMapper());
    }

    @Bean
    public JobManager jobManager() { return new JobManager(addressImportService()); }
}
