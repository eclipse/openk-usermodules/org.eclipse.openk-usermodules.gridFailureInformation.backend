variables:
  FF_USE_FASTZIP: "true" # enable fastzip - a faster zip implementation that also supports level configuration.
  ARTIFACT_COMPRESSION_LEVEL: fastest
  CACHE_COMPRESSION_LEVEL: fastest
  TRANSFER_METER_FREQUENCY: 5s # will display transfer progress every 5 seconds for artifacts and remote caches.
  MAVEN_OPTS: "-Djava.awt.headless=true -Dmaven.repo.local=${CI_PROJECT_DIR}/.m2/repository/"
  MAVEN_CLI_OPTS: "--batch-mode --errors --fail-at-end --show-version"
  SONAR_USER_HOME: ${CI_PROJECT_DIR}/.sonar  # Defines the location of the analysis task cache
  GIT_DEPTH: 0  # Tells git to fetch all the branches of the project, required by the analysis task
  DOCKER_BUILDKIT: 1 # With BuildKit, you don't need to pull the remote images before building since it caches each build layer in your image registry. Then, when you build the image, each layer is downloaded as needed during the build.
  #PACKAGE_REGISTRY_URL: "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/"
  # Since Packages are build across projects we use the static Project Id of the main backend: 30408479
  # Test
  MAIN_BACKEND_PROJECT_ID: "27814709"
  PACKAGE_REGISTRY_URL: "${CI_API_V4_URL}/projects/${MAIN_BACKEND_PROJECT_ID}/packages/generic/"

cache:
  key: ${CI_COMMIT_REF_SLUG}
  paths:
    - ${CI_PROJECT_DIR}/.m2/repository/

stages:

########################################################################################################################
# Parallel Stages
########################################################################################################################
  - Build
  - Test
  - Sonarqube
  - Dockerimage #Secure #Jobs flyway-build-main / docker-build-main

########################################################################################################################
# Non-Parallel Deploy-Stages
########################################################################################################################
  - Upload                      # only tags
  - Deploy-Flyway
  - Deploy-Main
  - Deploy-Address-Import
  - Deploy-Mail-Export
  - Deploy-Stoerungsauskuenfte
  - Release                   # only tags

########################################################################################################################
# Common stages/scripts
########################################################################################################################

.docker-build-script:
  image: docker:20.10.7-git
  services:
  - docker:dind
  before_script:
  - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script:
    - echo PROJECT_DIR $PROJECT_DIR
    - cd ./$PROJECT_DIR
    - ls -l
    - |
      if [[ "$CI_COMMIT_TAG" == "" ]]; then
        tag="sha-${CI_COMMIT_SHORT_SHA}"
      else
        tag="$CI_COMMIT_TAG"
        # Get (source) branch when tagging
        #CI_SOURCE_BRANCH=$(git for-each-ref | grep $CI_COMMIT_SHA | grep origin | sed "s/.*\///" | tr '[:upper:]' '[:lower:]')
      fi
    - echo current tag ${tag}
    - CI_SOURCE_BRANCH=$(echo $CI_COMMIT_BRANCH | tr '[:upper:]' '[:lower:]')
    - echo CI_SOURCE_BRANCH $CI_SOURCE_BRANCH
    - REGISTRY_IMAGE_BASE="$CI_REGISTRY_IMAGE/$CI_SOURCE_BRANCH/$IMG_NAME"
    - FINAL_REGISTRY_IMAGE="$CI_REGISTRY_IMAGE/$CI_SOURCE_BRANCH/$IMG_NAME:${tag}"
    - echo "CI_SOURCE_BRANCH=$CI_SOURCE_BRANCH" >> dockerimage.env
    - echo "FINAL_REGISTRY_IMAGE=$FINAL_REGISTRY_IMAGE" >> dockerimage.env
    - echo "REGISTRY_IMAGE_BASE=$REGISTRY_IMAGE_BASE" >> dockerimage.env
    - echo "IMAGE_TAG=$tag" >> dockerimage.env
    - echo current FINAL_REGISTRY_IMAGE ${FINAL_REGISTRY_IMAGE}
    - docker build --pull -f $DOCKER_FILE -t "$FINAL_REGISTRY_IMAGE" -t "$REGISTRY_IMAGE_BASE" --cache-from "$REGISTRY_IMAGE_BASE"  --build-arg BUILDKIT_INLINE_CACHE=1 .
    - docker push "$REGISTRY_IMAGE_BASE" --all-tags
  artifacts:
    reports:
      dotenv: $PROJECT_DIR/dockerimage.env

########################################################################################################################
# Main
########################################################################################################################

#-----------------------------------------------------------------------------------------------------------------------
build-main:
#-----------------------------------------------------------------------------------------------------------------------
  image: maven:3.9.8-eclipse-temurin-21
  stage: Build
  cache:
    key: ${CI_COMMIT_REF_SLUG}
    paths:
      - ${CI_PROJECT_DIR}/.m2/repository/
      - ./gfsBackendService/target/
  script:
    - cd ./gfsBackendService
    - mvn clean package -DskipTests=true
  artifacts:
    paths:
      - ./gfsBackendService/target/*.jar
    expire_in: 1d
  rules:
    - changes:
        - gfsBackendService/**/*
        - .gitlab-ci.yml
      if: $CI_PROJECT_NAME == "gridfailureinformation.backend"
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAME == "gridfailureinformation.backend"

#-----------------------------------------------------------------------------------------------------------------------
test-main:
#-----------------------------------------------------------------------------------------------------------------------
  stage: Test
  image: maven:3.9.8-eclipse-temurin-21
  script:
    - cd ./gfsBackendService
    - mvn test -Dskip.asciidoc=true
  artifacts:
    paths:
      - ./gfsBackendService/target/
#      - ./gfsBackendService/src/
    reports:
      junit:
        - ./gfsBackendService/target/surefire-reports/TEST-*.xml
  rules:
    - changes:
        - gfsBackendService/**/*
        - .gitlab-ci.yml
      if: $CI_PROJECT_NAME == "gridfailureinformation.backend"
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAME == "gridfailureinformation.backend"

#-----------------------------------------------------------------------------------------------------------------------
sonarqube-main:
#-----------------------------------------------------------------------------------------------------------------------
  stage: Sonarqube
  image:
    name: sonarsource/sonar-scanner-cli:5.0.1
    entrypoint: [""]
  cache:
    key: ${CI_JOB_NAME}
    paths:
      - .sonar/cache
  script:
    - cd ./gfsBackendService
    - sonar-scanner -Dsonar.qualitygate.wait=true
  allow_failure: true
  dependencies:
    - test-main
  rules:
    - changes:
        - gfsBackendService/**/*
        - .gitlab-ci.yml
      if: $CI_PROJECT_NAME == "gridfailureinformation.backend"
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAME == "gridfailureinformation.backend"

#-----------------------------------------------------------------------------------------------------------------------
docker-build-flyway:
#-----------------------------------------------------------------------------------------------------------------------
  stage: Dockerimage
  extends: .docker-build-script
  variables:
    IMG_NAME: "gfi-flyway"
    PROJECT_DIR: "gfsBackendService"
    DOCKER_FILE: "Dockerfile_Flyway"
  needs: []
  rules:
    - changes:
        - gfsBackendService/src/main/resources/db/migration/*
        - .gitlab-ci.yml
      exists:
        - gfsBackendService/Dockerfile_Flyway
      if: $CI_PROJECT_NAME == "gridfailureinformation.backend"
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAME == "gridfailureinformation.backend"

#-----------------------------------------------------------------------------------------------------------------------
docker-build-main:
#-----------------------------------------------------------------------------------------------------------------------
  stage: Dockerimage
  extends: .docker-build-script
  variables:
    IMG_NAME: "gfi-main"
    PROJECT_DIR: "gfsBackendService"
    DOCKER_FILE: "Dockerfile_GitLab"
  needs:
    - job: build-main
    - job: test-main
      artifacts: false
  rules:
    - changes:
        - gfsBackendService/**/*
        - .gitlab-ci.yml
      exists:
        - gfsBackendService/Dockerfile_GitLab
      if: $CI_PROJECT_NAME == "gridfailureinformation.backend"
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAME == "gridfailureinformation.backend"

########################################################################################################################
# Address Import
########################################################################################################################

#-----------------------------------------------------------------------------------------------------------------------
build-address-import:
#-----------------------------------------------------------------------------------------------------------------------
  image: maven:3.9.8-eclipse-temurin-21
  stage: Build
  cache:
    key: ${CI_COMMIT_REF_SLUG}
    paths:
      - ${CI_PROJECT_DIR}/.m2/repository/
      - ./addressImport/target/
  script:
    - cd ./addressImport
    - mvn clean package -DskipTests=true
  artifacts:
    paths:
      - ./addressImport/target/*.jar
    expire_in: 1d
  rules:
    - changes:
        - addressImport/**/*
        - .gitlab-ci.yml
      if: $CI_PROJECT_NAME == "gridfailureinformation.backend.addressimport"
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAME == "gridfailureinformation.backend.addressimport"

#-----------------------------------------------------------------------------------------------------------------------
test-address-import:
#-----------------------------------------------------------------------------------------------------------------------
  image: maven:3.9.8-eclipse-temurin-21
  stage:  Test
  script:
    - cd ./addressImport
    - mvn test
  artifacts:
    paths:
      - ./addressImport/target/
      - ./addressImport/src/
    reports:
      junit:
        - ./addressImport/target/surefire-reports/TEST-*.xml
  rules:
    - changes:
        - addressImport/**/*
        - .gitlab-ci.yml
      if: $CI_PROJECT_NAME == "gridfailureinformation.backend.addressimport"
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAME == "gridfailureinformation.backend.addressimport"

#-----------------------------------------------------------------------------------------------------------------------
sonarqube-address-import:
#-----------------------------------------------------------------------------------------------------------------------
  stage: Sonarqube
  image:
    name: sonarsource/sonar-scanner-cli:5.0.1
    entrypoint: [""]
  cache:
    key: ${CI_JOB_NAME}
    paths:
      - .sonar/cache
  script:
    - cd ./addressImport
    - sonar-scanner -Dsonar.qualitygate.wait=true
  allow_failure: true
  dependencies:
    - test-address-import
  rules:
    - changes:
        - addressImport/**/*
        - .gitlab-ci.yml
      if: $CI_PROJECT_NAME == "gridfailureinformation.backend.addressimport"
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAME == "gridfailureinformation.backend.addressimport"

#-----------------------------------------------------------------------------------------------------------------------
docker-build-address-import:
#-----------------------------------------------------------------------------------------------------------------------
  stage: Dockerimage
  extends: .docker-build-script
  variables:
    IMG_NAME: "gfi-address-import"
    PROJECT_DIR: "addressImport"
    DOCKER_FILE: "Dockerfile_Gitlab"
  needs:
    - job: build-address-import
    - job: test-address-import
      artifacts: false
  rules:
    - changes:
        - addressImport/**/*
        - .gitlab-ci.yml
      exists:
        - addressImport/Dockerfile_Gitlab
      if: $CI_PROJECT_NAME == "gridfailureinformation.backend.addressimport"
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAME == "gridfailureinformation.backend.addressimport"

########################################################################################################################
# Mail Export
########################################################################################################################
#-----------------------------------------------------------------------------------------------------------------------
build-mail-export:
#-----------------------------------------------------------------------------------------------------------------------
  image: maven:3.9.8-eclipse-temurin-21
  stage: Build
  cache:
    key: ${CI_COMMIT_REF_SLUG}
    paths:
      - ${CI_PROJECT_DIR}/.m2/repository/
      - ./mailExport/target/
  script:
    - cd ./mailExport
    - mvn clean package -DskipTests=true
  artifacts:
    paths:
      - ./mailExport/target/*.jar
  rules:
    - changes:
        - mailExport/**/*
        - .gitlab-ci.yml
      if: $CI_PROJECT_NAME == "gridfailureinformation.backend.mailexport"
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAME == "gridfailureinformation.backend.mailexport"

#-----------------------------------------------------------------------------------------------------------------------
test-mail-export:
#-----------------------------------------------------------------------------------------------------------------------
  image: maven:3.9.8-eclipse-temurin-21
  stage:  Test
  script:
    - cd ./mailExport
    - mvn test -Dskip.asciidoc=true
  artifacts:
    paths:
      - ./mailExport/target/
      - ./mailExport/src/
    reports:
      junit:
       - ./mailExport/target/surefire-reports/TEST-*.xml
  rules:
    - changes:
        - .gitlab-ci.yml
        - mailExport/**/*
      if: $CI_PROJECT_NAME == "gridfailureinformation.backend.mailexport"
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAME == "gridfailureinformation.backend.mailexport"

#-----------------------------------------------------------------------------------------------------------------------
sonarqube-mail-export:
#-----------------------------------------------------------------------------------------------------------------------
  stage: Sonarqube
  image:
    name: sonarsource/sonar-scanner-cli:5.0.1
    entrypoint: [""]
  cache:
    key: ${CI_JOB_NAME}
    paths:
      - .sonar/cache
  script:
    - cd ./mailExport
    - sonar-scanner -Dsonar.qualitygate.wait=true
  allow_failure: true
  dependencies:
    - test-mail-export
  rules:
    - changes:
        - mailExport/**/*
      if: $CI_PROJECT_NAME == "gridfailureinformation.backend.mailexport"
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAME == "gridfailureinformation.backend.mailexport"

#-----------------------------------------------------------------------------------------------------------------------
docker-build-mail-export:
#-----------------------------------------------------------------------------------------------------------------------
  stage: Dockerimage
  extends: .docker-build-script
  variables:
    IMG_NAME: "gfi-mail-export"
    PROJECT_DIR: "mailExport"
    DOCKER_FILE: "Dockerfile_Gitlab"
  needs:
    - job: build-mail-export
    - job: test-mail-export
      artifacts: false
  rules:
    - changes:
        - mailExport/**/*
        - .gitlab-ci.yml
      exists:
        - mailExport/Dockerfile_Gitlab
      if: $CI_PROJECT_NAME == "gridfailureinformation.backend.mailexport"
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAME == "gridfailureinformation.backend.mailexport"

########################################################################################################################
# SAMO Interface
########################################################################################################################
#-----------------------------------------------------------------------------------------------------------------------
build-samo:
#-----------------------------------------------------------------------------------------------------------------------
  image: maven:3.9.8-eclipse-temurin-21
  stage:  Build
  cache:
    key: ${CI_COMMIT_REF_SLUG}
    paths:
      - ${CI_PROJECT_DIR}/.m2/repository/
      - ./SAMO-Interface/target/
  script:
    - cd ./SAMO-Interface
    - mvn clean package -DskipTests=true
  artifacts:
    paths:
      - ./SAMO-Interface/target/*.jar
  rules:
    - changes:
        - SAMO-Interface/**/*
        - .gitlab-ci.yml
      if: $CI_PROJECT_NAME == "gridfailureinformation.backend.interface.samo"
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAME == "gridfailureinformation.backend.interface.samo"

#-----------------------------------------------------------------------------------------------------------------------
test-samo:
#-----------------------------------------------------------------------------------------------------------------------
  image: maven:3.9.8-eclipse-temurin-21
  stage:  Test
  script:
    - cd ./SAMO-Interface
    - mvn test -Dskip.asciidoc=true
  artifacts:
    paths:
      - ./SAMO-Interface/target/
      - ./SAMO-Interface/src/
    reports:
      junit:
        - ./SAMO-Interface/target/surefire-reports/TEST-*.xml
  rules:
    - changes:
        - SAMO-Interface/**/*
        - .gitlab-ci.yml
      if: $CI_PROJECT_NAME == "gridfailureinformation.backend.interface.samo"
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAME == "gridfailureinformation.backend.interface.samo"

#-----------------------------------------------------------------------------------------------------------------------
sonarqube-samo:
#-----------------------------------------------------------------------------------------------------------------------
  stage: Sonarqube
  image:
    name: sonarsource/sonar-scanner-cli:5.0.1
    entrypoint: [""]
  cache:
    key: ${CI_JOB_NAME}
    paths:
      - .sonar/cache
  script:
    - cd ./SAMO-Interface
    - sonar-scanner -Dsonar.qualitygate.wait=true
  allow_failure: true
  dependencies:
    - test-samo
  rules:
    - changes:
        - SAMO-Interface/**/*
        - .gitlab-ci.yml
      if: $CI_PROJECT_NAME == "gridfailureinformation.backend.interface.samo"
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAME == "gridfailureinformation.backend.interface.samo"

#-----------------------------------------------------------------------------------------------------------------------
docker-build-samo:
#-----------------------------------------------------------------------------------------------------------------------
  stage: Dockerimage
  extends: .docker-build-script
  variables:
    IMG_NAME: "gfi-samo-interface"
    PROJECT_DIR: "SAMO-Interface"
    DOCKER_FILE: "Dockerfile_GitLab"
  needs:
    - job: build-samo
    - job: test-samo
      artifacts: false
  rules:
    - changes:
        - SAMO-Interface/**/*
        - .gitlab-ci.yml
      exists:
        - SAMO-Interface/Dockerfile_GitLab
      if: $CI_PROJECT_NAME == "gridfailureinformation.backend.interface.samo"
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAME == "gridfailureinformation.backend.interface.samo"


########################################################################################################################
# SARIS Interface
########################################################################################################################
#-----------------------------------------------------------------------------------------------------------------------
build-saris:
#-----------------------------------------------------------------------------------------------------------------------
  image: maven:3.9.8-eclipse-temurin-21
  stage:  Build
  cache:
    key: ${CI_COMMIT_REF_SLUG}
    paths:
      - ${CI_PROJECT_DIR}/.m2/repository/
      - ./SARIS-Interface/target/
  script:
    - cd ./SARIS-Interface
    - mvn clean package -DskipTests=true
  artifacts:
    paths:
      - ./SARIS-Interface/target/*.jar
  rules:
    - changes:
        - SARIS-Interface/**/*
        - .gitlab-ci.yml
      if: $CI_PROJECT_NAME == "gridfailureinformation.backend.interface.saris"
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAME == "gridfailureinformation.backend.interface.saris"

#-----------------------------------------------------------------------------------------------------------------------
test-saris:
#-----------------------------------------------------------------------------------------------------------------------
  image: maven:3.9.8-eclipse-temurin-21
  stage:  Test
  script:
    - cd ./SARIS-Interface
    - mvn test -Dskip.asciidoc=true
  artifacts:
    paths:
      - ./SARIS-Interface/target/
      - ./SARIS-Interface/src/
    reports:
      junit:
        - ./SARIS-Interface/target/surefire-reports/TEST-*.xml
  rules:
    - changes:
        - SARIS-Interface/**/*
        - .gitlab-ci.yml
      if: $CI_PROJECT_NAME == "gridfailureinformation.backend.interface.saris"
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAME == "gridfailureinformation.backend.interface.saris"

#-----------------------------------------------------------------------------------------------------------------------
sonarqube-saris:
#-----------------------------------------------------------------------------------------------------------------------
  stage: Sonarqube
  image:
    name: sonarsource/sonar-scanner-cli:5.0.1
    entrypoint: [""]
  cache:
    key: ${CI_JOB_NAME}
    paths:
      - .sonar/cache
  script:
    - cd ./SARIS-Interface
    - sonar-scanner -Dsonar.qualitygate.wait=true
  allow_failure: true
  dependencies:
    - test-saris
  rules:
    - changes:
        - SARIS-Interface/**/*
        - .gitlab-ci.yml
      if: $CI_PROJECT_NAME == "gridfailureinformation.backend.interface.saris"
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAME == "gridfailureinformation.backend.interface.saris"

#-----------------------------------------------------------------------------------------------------------------------
docker-build-saris:
#-----------------------------------------------------------------------------------------------------------------------
  stage: Dockerimage
  extends: .docker-build-script
  variables:
    IMG_NAME: "gfi-saris-interface"
    PROJECT_DIR: "SARIS-Interface"
    DOCKER_FILE: "Dockerfile_GitLab"
  needs:
    - job: build-saris
    - job: test-saris
      artifacts: false
  rules:
    - changes:
        - SARIS-Interface/**/*
        - .gitlab-ci.yml
      exists:
        - SARIS-Interface/Dockerfile_GitLab
      if: $CI_PROJECT_NAME == "gridfailureinformation.backend.interface.saris"
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAME == "gridfailureinformation.backend.interface.saris"


########################################################################################################################
# Störungsauskünfte
########################################################################################################################
#-----------------------------------------------------------------------------------------------------------------------
build-stoerungsauskuenfte:
#-----------------------------------------------------------------------------------------------------------------------
  image: maven:3.9.8-eclipse-temurin-21
  stage:  Build
  cache:
    key: ${CI_COMMIT_REF_SLUG}
    paths:
      - ${CI_PROJECT_DIR}/.m2/repository/
      - ./stoerungsauskunftInterface/target/
  script:
    - cd ./stoerungsauskunftInterface
    - mvn clean package -DskipTests=true
  artifacts:
    paths:
      - ./stoerungsauskunftInterface/target/*.jar
  rules:
    - changes:
        - stoerungsauskunftInterface/**/*
        - .gitlab-ci.yml
      if: $CI_PROJECT_NAME == "gridfailureinformation.backend.interface.stoerungsauskunft"
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAME == "gridfailureinformation.backend.interface.stoerungsauskunft"

#-----------------------------------------------------------------------------------------------------------------------
test-stoerungsauskuenfte:
#-----------------------------------------------------------------------------------------------------------------------
  image: maven:3.9.8-eclipse-temurin-21
  stage:  Test
  script:
    - cd ./stoerungsauskunftInterface
    - mvn test -Dskip.asciidoc=true
  artifacts:
    paths:
      - ./stoerungsauskunftInterface/target/
      - ./stoerungsauskunftInterface/src/
    reports:
      junit:
        - ./stoerungsauskunftInterface/target/surefire-reports/TEST-*.xml
  rules:
    - changes:
        - stoerungsauskunftInterface/**/*
        - .gitlab-ci.yml
      if: $CI_PROJECT_NAME == "gridfailureinformation.backend.interface.stoerungsauskunft"
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAME == "gridfailureinformation.backend.interface.stoerungsauskunft"

#-----------------------------------------------------------------------------------------------------------------------
sonarqube-stoerungsauskuenfte:
#-----------------------------------------------------------------------------------------------------------------------
  stage: Sonarqube
  image:
    name: sonarsource/sonar-scanner-cli:5.0.1
    entrypoint: [""]
  cache:
    key: ${CI_JOB_NAME}
    paths:
      - .sonar/cache
  script:
    - echo ${CI_PROJECT_DIR}
    - cd ./stoerungsauskunftInterface
    - sonar-scanner -Dsonar.qualitygate.wait=true
  allow_failure: true
  dependencies:
    - test-stoerungsauskuenfte
  rules:
    - changes:
        - stoerungsauskunftInterface/**/*
        - .gitlab-ci.yml
      if: $CI_PROJECT_NAME == "gridfailureinformation.backend.interface.stoerungsauskunft"
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAME == "gridfailureinformation.backend.interface.stoerungsauskunft"

#-----------------------------------------------------------------------------------------------------------------------
docker-build-stoerungsauskuenfte:
#-----------------------------------------------------------------------------------------------------------------------
  stage: Dockerimage
  extends: .docker-build-script
  variables:
    IMG_NAME: "gfi-stoerungsauskunft-interface"
    PROJECT_DIR: "stoerungsauskunftInterface"
    DOCKER_FILE: "Dockerfile_GitLab"
  needs:
    - job: build-stoerungsauskuenfte
    - job: test-stoerungsauskuenfte
      artifacts: false
  rules:
    - changes:
        - stoerungsauskunftInterface/**/*
        - .gitlab-ci.yml
      exists:
        - stoerungsauskunftInterface/Dockerfile_GitLab
      if: $CI_PROJECT_NAME == "gridfailureinformation.backend.interface.stoerungsauskunft"
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAME == "gridfailureinformation.backend.interface.stoerungsauskunft"


#-----------------------------------------------------------------------------------------------------------------------
# Deploy
#-----------------------------------------------------------------------------------------------------------------------
.deploy-script:
  image: alpine:3.14.0
  cache: {}
  variables:
    ONLY_MASTER: "false"
    GIT_STRATEGY: none
    DEPLOYMENT_FILE: fileDefaultVarPlaceholder
    YAML_IMAGE_NAME: image
  before_script:
    - apk add --no-cache git curl bash coreutils
    - wget https://github.com/mikefarah/yq/releases/download/v4.17.2/yq_linux_amd64 -O /usr/bin/yq && chmod +x /usr/bin/yq
    - yq -V
    - ls -l
    - git clone https://${CI_USERNAME}:${CI_PUSH_TOKEN}@gitlab.com/${GITLAB_DEPLOYMENT_REPO_URL}
    - cd *
    - git config --global user.email "gitlab@gitlab.com"
    - git config --global user.name "GitLab CI/CD"
  script:
    - echo "CI_SOURCE_BRANCH=$CI_SOURCE_BRANCH"
    - ls -l
    # - |
    #   if [[ "$CI_SOURCE_BRANCH" != "master" && "$ONLY_MASTER" == "true" ]]; then
    #     exit 0;
    #   fi
    - cat ${DEPLOYMENT_FILE}
    - echo DEPLOYMENT_FILE ${DEPLOYMENT_FILE}
    - echo FINAL_REGISTRY_IMAGE ${FINAL_REGISTRY_IMAGE}
    - echo YAML_APP_NAME ${YAML_APP_NAME}
    - echo YAML_IMAGE_NAME ${YAML_IMAGE_NAME}
    - echo IMAGE_TAG ${IMAGE_TAG}
    - echo REGISTRY_IMAGE_BASE ${REGISTRY_IMAGE_BASE}
    - yq e -i '.apps.[env(YAML_APP_NAME)][env(YAML_IMAGE_NAME)].tag = env(IMAGE_TAG)' ${DEPLOYMENT_FILE}
    - yq e -i '.apps.[env(YAML_APP_NAME)][env(YAML_IMAGE_NAME)].repository = env(REGISTRY_IMAGE_BASE)' ${DEPLOYMENT_FILE}
    - cat ${DEPLOYMENT_FILE}
    #- git commit -am '[skip ci] Image update'
    - git diff
    - |
      if ! git diff --quiet; then
        git commit -am '[skip ci] Image update'
        exit_code=1; attempts=0; max_retry=10;
        set +e # Disable exit on error
        until [ $exit_code -eq 0 ]
        do
          let ++attempts;
          [[ attempts -ne 1 ]] && sleep 3
          echo attempt $attempts
          now=$(date) && echo $now
          git pull --rebase;
          git push origin main;
          exit_code=$?;
          [[ attempts -eq $max_retry ]] && echo "Gave up after $attempts attempts" && break
        done;
        set -e
      else
        echo "No changes found."
      fi


#-----------------------------------------------------------------------------------------------------------------------
# Upload Artefakte BE
#-----------------------------------------------------------------------------------------------------------------------
upload_artefacts:
  stage: Upload
  image: alpine:3.14.0
  rules:
    - if: $CI_COMMIT_TAG
  before_script:
    - apk add --no-cache git curl bash coreutils zip
  script:
    - echo PACKAGE_REGISTRY_URL $PACKAGE_REGISTRY_URL
    - echo CI_COMMIT_TAG $CI_COMMIT_TAG

    - |
      if [[ -f ./addressImport/target/grid-failure-information.addressimport.service.jar ]]; then
        cp ./addressImport/target/grid-failure-information.addressimport.service.jar ./addressimport.service.jar
        cp ./addressImport/src/main/resources/application.yml ./addressimport-application.yml
        zip -r addressimport.zip addressimport.service.jar addressimport-application.yml
        curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file ./addressimport.zip "${PACKAGE_REGISTRY_URL}${CI_COMMIT_TAG}/artefakte/addressimport.zip"
      fi

    - |
      if [[ -f ./gfsBackendService/target/grid-failure-information.backend.service.jar ]]; then
        cp ./gfsBackendService/target/grid-failure-information.backend.service.jar ./backend.service.jar
        cp ./gfsBackendService/src/main/resources/application.yml ./main-application.yml
        zip -r main.zip backend.service.jar main-application.yml
        curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file ./main.zip "${PACKAGE_REGISTRY_URL}${CI_COMMIT_TAG}/artefakte/main.zip"
      fi

    - |
      if [[ -f ./mailExport/target/grid-failure-information.mailexport.jar ]]; then
        cp ./mailExport/target/grid-failure-information.mailexport.jar ./mailexport.jar
        cp ./mailExport/src/main/resources/application.yml ./mailexport-application.yml
        zip -r mailexport.zip mailexport.jar mailexport-application.yml
        curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file ./mailexport.zip "${PACKAGE_REGISTRY_URL}${CI_COMMIT_TAG}/artefakte/mailexport.zip"
      fi

    - |
      if [[ -f ./SAMO-Interface/target/grid-failure-information.backend.samo-interface.jar ]]; then
        cp ./SAMO-Interface/target/grid-failure-information.backend.samo-interface.jar ./samo-interface.jar
        cp ./SAMO-Interface/src/main/resources/application.yml ./samo-application.yml
        zip -r samo-interface.zip samo-interface.jar samo-application.yml
        curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file ./samo-interface.zip "${PACKAGE_REGISTRY_URL}${CI_COMMIT_TAG}/artefakte/samo-interface.zip"
      fi

    - |
      if [[ -f ./SARIS-Interface/target/grid-failure-information.backend.saris-interface.jar ]]; then
        cp ./SARIS-Interface/target/grid-failure-information.backend.saris-interface.jar ./saris-interface.jar
        cp ./SARIS-Interface/src/main/resources/application.yml ./saris-application.yml
        zip -r saris-interface.zip saris-interface.jar saris-application.yml
        curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file ./saris-interface.zip "${PACKAGE_REGISTRY_URL}${CI_COMMIT_TAG}/artefakte/saris-interface.zip"
      fi

    - |
      if [[ -f ./stoerungsauskunftInterface/target/grid-failure-information.backend.stoerungsauskunft-interface.jar ]]; then
        cp ./stoerungsauskunftInterface/target/grid-failure-information.backend.stoerungsauskunft-interface.jar ./stoerungsauskunft-interface.jar
        cp ./stoerungsauskunftInterface/src/main/resources/application.yml ./stoerungsauskunft-application.yml
        zip -r stoerungsauskunft-interface.zip stoerungsauskunft-interface.jar stoerungsauskunft-application.yml
        curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file ./stoerungsauskunft-interface.zip "${PACKAGE_REGISTRY_URL}${CI_COMMIT_TAG}/artefakte/stoerungsauskunft-interface.zip"
      fi

#------------------------------
# Deploy - QA-Environment
#------------------------------
deploy-qa-flyway:
  stage: Deploy-Flyway
  extends: .deploy-script
  variables:
    ONLY_MASTER: "true"
    YAML_APP_NAME: gfi-be
    YAML_IMAGE_NAME: imageDatabase
    DEPLOYMENT_FILE: deployment/applications/values-gfi-qa.yaml
  dependencies:
    - docker-build-flyway
  rules:
    - if: $CI_COMMIT_BRANCH == "master" && $CI_PROJECT_NAME == "gridfailureinformation.backend"
      changes:
        - Dockerfile_Flyway
        - src/main/resources/db/migration/*
        - .gitlab-ci.yml
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAME == "gridfailureinformation.backend"

deploy-qa-main:
  stage: Deploy-Main
  extends: .deploy-script
  variables:
    ONLY_MASTER: "true"
    YAML_APP_NAME: gfi-be
    DEPLOYMENT_FILE: deployment/applications/values-gfi-qa.yaml
  dependencies:
    - docker-build-main
  rules:
    - changes:
        - gfsBackendService/**/*
        - .gitlab-ci.yml
      if: $CI_COMMIT_BRANCH == "master" && $CI_PROJECT_NAME == "gridfailureinformation.backend"
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAME == "gridfailureinformation.backend"

deploy-qa-address-import:
  stage: Deploy-Address-Import
  extends: .deploy-script
  variables:
    ONLY_MASTER: "true"
    YAML_APP_NAME: gfi-address-import
    DEPLOYMENT_FILE: deployment/applications/values-gfi-qa.yaml
  dependencies:
    - docker-build-address-import
  rules:
    - changes:
        - addressImport/**/*
        - .gitlab-ci.yml
      if: $CI_PROJECT_NAME == "gridfailureinformation.backend.addressimport" && $CI_COMMIT_BRANCH == "master"
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAME == "gridfailureinformation.backend.addressimport"

deploy-qa-mail-export:
  stage: Deploy-Mail-Export
  extends: .deploy-script
  variables:
    ONLY_MASTER: "true"
    YAML_APP_NAME: gfi-mail-export
    DEPLOYMENT_FILE: deployment/applications/values-gfi-qa.yaml
  dependencies:
    - docker-build-mail-export
  rules:
    - changes:
        - mailExport/**/*
        - .gitlab-ci.yml
      if: $CI_PROJECT_NAME == "gridfailureinformation.backend.mailexport" && $CI_COMMIT_BRANCH == "master"
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAME == "gridfailureinformation.backend.mailexport"

deploy-qa-stoerungsauskunft-interface:
  stage: Deploy-Stoerungsauskuenfte
  extends: .deploy-script
  variables:
    ONLY_MASTER: "true"
    YAML_APP_NAME: gfi-stoerungsauskunft
    DEPLOYMENT_FILE: deployment/applications/values-gfi-qa.yaml
  dependencies:
    - docker-build-stoerungsauskuenfte
  rules:
    - changes:
        - stoerungsauskunftInterface/**/*
        - .gitlab-ci.yml
      exists:
        - stoerungsauskunftInterface/Dockerfile_GitLab
      if: $CI_PROJECT_NAME == "gridfailureinformation.backend.interface.stoerungsauskunft" && $CI_COMMIT_BRANCH == "master"
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAME == "gridfailureinformation.backend.interface.stoerungsauskunft"

#------------------------------
# Deploy - DEV-Environment
#------------------------------
deploy-dev-flyway:
  stage: Deploy-Flyway
  extends: .deploy-script
  variables:
    YAML_APP_NAME: gfi-be
    YAML_IMAGE_NAME: imageDatabase
    DEPLOYMENT_FILE: deployment/applications/values-gfi-dev.yaml
  dependencies:
    - docker-build-flyway
  rules:
    - if: $CI_PROJECT_NAME == "gridfailureinformation.backend" && $CI_COMMIT_BRANCH == "DEVELOP"
      changes:
        - Dockerfile_Flyway
        - src/main/resources/db/migration/*
        - .gitlab-ci.yml

deploy-dev-main:
  stage: Deploy-Main
  extends: .deploy-script
  variables:
    YAML_APP_NAME: gfi-be
    DEPLOYMENT_FILE: deployment/applications/values-gfi-dev.yaml
  dependencies:
    - docker-build-main
  rules:
    - changes:
        - gfsBackendService/**/*
        - .gitlab-ci.yml
      if: $CI_PROJECT_NAME == "gridfailureinformation.backend" && $CI_COMMIT_BRANCH == "DEVELOP"

deploy-dev-address-import:
  stage: Deploy-Address-Import
  extends: .deploy-script
  variables:
    YAML_APP_NAME: gfi-address-import
    DEPLOYMENT_FILE: deployment/applications/values-gfi-dev.yaml
  dependencies:
    - docker-build-address-import
  rules:
    - changes:
        - addressImport/**/*
        - .gitlab-ci.yml
      if: $CI_PROJECT_NAME == "gridfailureinformation.backend.addressimport" && $CI_COMMIT_BRANCH == "DEVELOP"

deploy-dev-mail-export:
  stage: Deploy-Mail-Export
  extends: .deploy-script
  variables:
    YAML_APP_NAME: gfi-mail-export
    DEPLOYMENT_FILE: deployment/applications/values-gfi-dev.yaml
  dependencies:
    - docker-build-mail-export
  rules:
    - changes:
        - mailExport/**/*
        - .gitlab-ci.yml
      if: $CI_PROJECT_NAME == "gridfailureinformation.backend.mailexport" && $CI_COMMIT_BRANCH == "DEVELOP"

deploy-dev-stoerungsauskunft-interface:
  stage: Deploy-Stoerungsauskuenfte
  extends: .deploy-script
  variables:
    YAML_APP_NAME: gfi-stoerungsauskunft
    DEPLOYMENT_FILE: deployment/applications/values-gfi-dev.yaml
  dependencies:
    - docker-build-stoerungsauskuenfte
  rules:
    - changes:
        - stoerungsauskunftInterface/**/*
        - .gitlab-ci.yml
      if: $CI_PROJECT_NAME == "gridfailureinformation.backend.interface.stoerungsauskunft" && $CI_COMMIT_BRANCH == "DEVELOP"

#------------------------------
# Release
#------------------------------
release_job:
  stage: Release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  needs:
    - job: upload_artefacts
      artifacts: false
  script:
    - echo 'running release_job'
  release:
    name: 'Release $CI_COMMIT_TAG'
    description: 'Created using the release-cli'
    tag_name: '$CI_COMMIT_TAG'
    ref: '$CI_COMMIT_SHA'
    assets:
      links:
        - name: 'addressimport.zip (Download)'
          url:   "${PACKAGE_REGISTRY_URL}${CI_COMMIT_TAG}/artefakte/addressimport.zip"
        - name: 'main.zip (Download)'
          url:   "${PACKAGE_REGISTRY_URL}${CI_COMMIT_TAG}/artefakte/main.zip"
        - name: 'mailexport.zip (Download)'
          url:   "${PACKAGE_REGISTRY_URL}${CI_COMMIT_TAG}/artefakte/mailexport.zip"
        - name: 'samo-interface.zip (Download)'
          url:   "${PACKAGE_REGISTRY_URL}${CI_COMMIT_TAG}/artefakte/samo-interface.zip"
        - name: 'saris-interface.zip (Download)'
          url:   "${PACKAGE_REGISTRY_URL}${CI_COMMIT_TAG}/artefakte/saris-interface.zip"
        - name: 'stoerungsauskunft-interface.zip (Download)'
          url:   "${PACKAGE_REGISTRY_URL}${CI_COMMIT_TAG}/artefakte/stoerungsauskunft-interface.zip"
  rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_NAME == "gridfailureinformation.backend"